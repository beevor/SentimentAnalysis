import os

'''Global path variables are set in here.'''
'''Be aware that a change of this file might affect several other files.'''

# TODO: A sufficient size for mini-batches might be 32, 64, 128 due to https://datascience.stackexchange.com/questions/18414/are-there-any-rules-for-choosing-the-size-of-a-mini-batch

# Data directories
global_path_to_data = '../data/'
global_path_to_database = '../data/comments.sqlite'
global_path_to_reformatted_comments = '../data/reformattedComments'

